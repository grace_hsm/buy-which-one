//
//  main.m
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
