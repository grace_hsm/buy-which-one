//
//  MainVC.m
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import "MainVC.h"
#import "ReceiptVC.h"
#import "Product.h"
#import "DataManager.h"
#import "MBProgressHUD.h"
#import "UPCDatabase.h"
#import "AdsVC.h"
#import "WToast.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

#define kAlertTagChangeType 100
#define kAlertTagCurrencySymbol 101

@interface MainVC ()

@end

@implementation MainVC

DataManager *dm;
UIButton *selectedTypeButton;
ReceiptVC *selectedVC;

- (void)enableReset
{
    resetButton.enabled = YES;
}

- (float)getUnitMultiplyFromName:(NSString *)unitName Type:(NSString *)type
{
    NSMutableArray *tempArr;
    if([type isEqualToString:TypeWeight])
        tempArr = compareWeightArr;
    else if([type isEqualToString:TypeVolume])
        tempArr = compareVolumeArr;
    else if([type isEqualToString:TypeQty])
        tempArr = compareQtyArr;
    
    for(NSDictionary *dic in tempArr)
    {
        if([[dic objectForKey:@"UnitName"] isEqualToString:unitName])
        {
            return [[dic objectForKey:@"UnitMultiply"] floatValue];
        }
    }
    
    
    return 0.0;
}

- (void)showAds
{
    if(dm.showAds)
    {
    UIStoryboard* storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AdsVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdsVC"];
    
    [self presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)compareClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Compare"          // Event label
                                                           value:nil] build]];

    [self endEditing];
    [self showAds];
    
    
    NSString *lowerUnitName = @"";
    float lowerUnitMultiply = 0.0;
    for(ReceiptVC *vc in receiptArray)
    {
        Product *product = vc.product;
        if(lowerUnitName.length == 0)
        {
            lowerUnitName = product.UnitName;
            lowerUnitMultiply = [self getUnitMultiplyFromName:product.UnitName Type:product.UnitType];
            continue;
        }
        
        float cMultiply = [self getUnitMultiplyFromName:product.UnitName Type:product.UnitType];
        if(cMultiply < lowerUnitMultiply)
        {
            lowerUnitName = product.UnitName;
            lowerUnitMultiply = [self getUnitMultiplyFromName:product.UnitName Type:product.UnitType];
        }
    }
    
    //show result
    for(ReceiptVC *vc in receiptArray)
    {
        Product *product = vc.product;
        product.UnitNameResult = lowerUnitName;
        float cMultiply = [self getUnitMultiplyFromName:product.UnitName Type:product.UnitType];
        product.PriceResult = [NSNumber numberWithFloat:product.Price.floatValue / (product.UnitAmount.floatValue * cMultiply / lowerUnitMultiply)];
        NSLog(@"%f%@ = %f%@  --->   (%f * %f / %f)", product.UnitAmount.floatValue, product.UnitName, product.UnitAmount.floatValue * cMultiply / lowerUnitMultiply, lowerUnitName, product.UnitAmount.floatValue, cMultiply, lowerUnitMultiply);
    }
    
    for(ReceiptVC *vc in receiptArray)
    {
        int rank = 1;
        for(ReceiptVC *vc2 in receiptArray)
        {
            if(vc2 == vc)
                continue;
            if(vc.product.PriceResult.floatValue > vc2.product.PriceResult.floatValue)
                rank ++;
        }
        
        vc.product.Ranking = [NSNumber numberWithInt:rank];
        [vc setCrown:vc.product.Ranking.intValue];
        
    }
    
    compareButton.enabled = NO;
}

- (IBAction)resetClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Reset"          // Event label
                                                           value:nil] build]];
    
    [self endEditing];
    for(ReceiptVC *vc in receiptArray)
    {
        [vc.view removeFromSuperview];
    }
    //[receiptArray removeAllObjects];
    receiptArray = [[NSMutableArray alloc] init];
    
    [self confirmKeyboardTypeClicked:keyboardButtonTypeWgt];
    [self addReceipt:nil];
    [self addReceipt:nil];
    
    resetButton.enabled = NO;
    compareButton.enabled = NO;
    plusButton.enabled = YES;
}

- (void)loadData:(NSArray *)arr
{
    [self endEditing];
    for(ReceiptVC *vc in receiptArray)
    {
        [vc.view removeFromSuperview];
    }
    receiptArray = [[NSMutableArray alloc] init];
    
    int i = 0;
    for(Product *product in arr)
    {
        if(i == 0)
        {
            if([product.UnitType isEqualToString:TypeWeight])
                [self confirmKeyboardTypeClicked:keyboardButtonTypeWgt];
            else if([product.UnitType isEqualToString:TypeVolume])
                [self confirmKeyboardTypeClicked:keyboardButtonTypeVol];
            else if([product.UnitType isEqualToString:TypeQty])
                [self confirmKeyboardTypeClicked:keyboardButtonTypeQty];
        }
        [self addReceipt:product];
    }
    
    resetButton.enabled = YES;
//    compareButton.enabled = NO;
    [self updateCompareButton];
    plusButton.enabled = (receiptArray.count >= 5) ? NO : YES;
}

- (IBAction)soundClicked:(id)sender
{
    
}
- (IBAction)currencyClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Currency"          // Event label
                                                           value:nil] build]];

    [self endEditing];
    [self hideAllResult];
    [self updateCompareButton];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Currency Symbol" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.tag = kAlertTagCurrencySymbol;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textfield = [alert textFieldAtIndex:0];
    textfield.placeholder = @"Currency Symbol";
    textfield.text = [dm getCurrency];
    textfield.delegate = self;

    [alert show];
}

- (IBAction)feedbackClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Feedback"          // Event label
                                                           value:nil] build]];

    NSString *subject = [NSString stringWithFormat:@"Feedback for %@ v%@", GameTitle, [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    [self sendMailTo:@"dotstripes@yahoo.com" subject:subject body:nil];
}
- (IBAction)shareClicked:(id)sender
{
    
}
- (IBAction)aboutClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"About"          // Event label
                                                           value:nil] build]];

    NSString *title = [NSString stringWithFormat:@"%@\nVersion %@", GameTitle, [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:title
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

}

- (void)removeReceipt:(ReceiptVC *)vc
{
    if(!grayView.hidden) //editing
        return;
    
    UIView *vw = vc.view;
    float prevX = vw.frame.origin.x;
    float prevY = vw.frame.origin.y;
    
//    [UIView beginAnimations:@"fade in" context:nil];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//    CGRect frame = vw.frame;
//    frame.origin.y -= receiptsView.frame.size.height;
//    vw.frame = frame;
//    [UIView commitAnimations];
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void) {
        CGRect frame = vw.frame;
        frame.origin.y -= receiptsView.frame.size.height;
        vw.frame = frame;
    } completion:^(BOOL finished) {
        [vc.view removeFromSuperview];
        [receiptArray removeObject:vc];
        
        if(receiptArray.count > 0)
        {
            ReceiptVC *lastRec = [receiptArray lastObject];
//            NSLog(@"last: %@ %@", lastRec.productNameTextfield.text, NSStringFromCGRect(lastRec.view.frame));
            float newWidth = (MAX(lastRec.view.frame.origin.x+lastRec.view.frame.size.width, receiptsView.frame.size.width+1));
                              
//            NSLog(@"new content size: %@", NSStringFromCGSize(receiptsView.contentSize));
//                [receiptsView scrollRectToVisible:CGRectMake(receiptsView.contentSize.width - 10, 0, 10, 10) animated:YES];
            if(receiptsView.contentOffset.x > newWidth - receiptsView.frame.size.width)
                [receiptsView setContentOffset:CGPointMake(newWidth - receiptsView.frame.size.width, 0) animated:YES];
//            NSNumber *number = [NSNumber numberWithFloat:newWidth];
            [self performSelector:@selector(updateContentSize) withObject:nil afterDelay:0.3];
        }
        plusButton.enabled = YES;
        
    }];
    
    int index = (int) [receiptArray indexOfObject:vc];
    for(int i = index+1; i<receiptArray.count; i++)
    {
        ReceiptVC *nextVC = [receiptArray objectAtIndex:i];
        UIView *nextView = nextVC.view;
        float tempX = prevX;
        float tempY = prevY;
        prevX = nextView.frame.origin.x;
        prevY = nextView.frame.origin.y;
        
        
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelay:0.2];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        CGRect frame = nextView.frame;
        frame.origin.x = tempX;
        frame.origin.y = tempY;
        nextView.frame = frame;
        [UIView commitAnimations];
        /*[UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveLinear animations:^(void) {
            CGRect frame = nextView.frame;
            frame.origin.x = temp;
            nextView.frame = frame;

        } completion:^(BOOL finished) {
            [vc.view removeFromSuperview];
            [receiptArray removeObject:vc];
            
            if(receiptArray.count > 0)
            {
                ReceiptVC *lastRec = [receiptArray lastObject];
                NSLog(@"last: %@", lastRec.productNameTextfield.text);
                receiptsView.contentSize = CGSizeMake(MAX(lastRec.view.frame.origin.x+lastRec.view.frame.size.width -receiptsView.frame.size.width, receiptsView.frame.size.width+1), receiptsView.frame.size.height);
                if(receiptsView.contentOffset.x > receiptsView.contentSize.width - receiptsView.frame.size.width)
                    [receiptsView scrollRectToVisible:CGRectMake(receiptsView.contentSize.width - 10, 0, 10, 10) animated:YES];
            }
            plusButton.enabled = YES;
            
        }];*/
        
    }
}

- (void)updateContentSize
{
    //receiptsView.contentSize = CGSizeMake(width.floatValue, receiptsView.frame.size.height);
    ReceiptVC *lastRec = [receiptArray lastObject];
    NSLog(@"last: %@ %@", lastRec.productNameTextfield.text, NSStringFromCGRect(lastRec.view.frame));
    float newWidth = (MAX(lastRec.view.frame.origin.x+lastRec.view.frame.size.width, receiptsView.frame.size.width+1));
    receiptsView.contentSize = CGSizeMake(newWidth, receiptsView.frame.size.height);
    
    [self updateCompareButton];
}

- (IBAction)plusClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Plus"          // Event label
                                                           value:nil] build]];

    [self addReceipt:nil];
}

- (void)addReceipt:(Product *)newProd
{
    UIStoryboard* storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ReceiptVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ReceiptVC"];
    
    float prevY = -1;
    int index = 0;
    if(receiptArray.count > 0)
    {
        ReceiptVC *prev = [receiptArray lastObject];
        prevY = prev.view.frame.origin.y;
        index = (int)prev.view.tag-10+1;
    }
    
    vc.delegate = self;
    vc.view.tag = index+10;
    vc.view.backgroundColor = [UIColor clearColor];
    //int asciiCode = 65+(int)receiptArray.count; //65 = A
    int asciiCode = 65+index; //65 = A
    
    Product *product;
    if(!newProd)
    {
        product = [[Product alloc] init];
        product.PlaceHolder = (asciiCode >= 65+26 ? [NSString stringWithFormat:@"Product %d", index-25] : [NSString stringWithFormat:@"Product %c", asciiCode]);
        product.Name = @"";
        product.Description = @"";
        product.Discount = [NSNumber numberWithInt:0];
        product.Price = [NSNumber numberWithFloat:0.00];
        product.UnitAmount = [NSNumber numberWithFloat:10];
        //product.codeString = @"8992994110112";
        product.codeString = @"XXXXXXXXXXXXX";
        if(keyboardButtonTypeWgt.selected)
        {
            product.UnitType = TypeWeight;
            product.UnitName = @"g";
        }
        else if(keyboardButtonTypeVol.selected)
        {
            product.UnitType = TypeVolume;
            product.UnitName = @"cc";
        }
        else if(keyboardButtonTypeQty.selected)
        {
            product.UnitType = TypeQty;
            product.UnitName = @"pcs";
        }
        vc.product = product;
    }
    else
        vc.product = newProd;
    
    
    [receiptArray addObject:vc];
    [receiptsView addSubview:vc.view];
    
//    [vc setProductName:product.Name];
//    [vc setDiscount:0];
//    [vc setPrice:250.00];

    CGRect frame = vc.view.frame;
    frame.size.width = 172;
    frame.size.height = 282;
    frame.origin.x = 90+(frame.size.width - 20) * (receiptArray.count-1);
    do {
        frame.origin.y = ((arc4random()%11) - 1) *5;
    } while (frame.origin.y == prevY || frame.origin.y > 100 || frame.origin.y < - 20);
//    frame.origin.y = (10 - 1) *5;
    frame.origin.x += receiptsView.frame.size.width;
    vc.view.frame = frame;
//    NSLog(@"vc:[%lu] %@", (unsigned long)receiptArray.count, NSStringFromCGRect(frame));
    
    receiptsView.contentSize = CGSizeMake(MAX(vc.view.frame.origin.x+vc.view.frame.size.width -receiptsView.frame.size.width, receiptsView.frame.size.width+1), receiptsView.frame.size.height);
    [receiptsView scrollRectToVisible:CGRectMake(receiptsView.contentSize.width - 10, 0, 10, 10) animated:YES];
    
    [self performSelector:@selector(slideIn:) withObject:vc.view afterDelay:0.1];
//    NSLog(@"size: %@", NSStringFromCGSize(receiptsView.contentSize));
    
    if(receiptArray.count >= 5)
        plusButton.enabled = NO;
    [self enableReset];
    [self updateCompareButton];
}

- (void)slideIn:(UIView *)vw
{
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.3];
    CGRect frame = vw.frame;
    frame.origin.x -= receiptsView.frame.size.width;
    vw.frame = frame;
    [UIView commitAnimations];
}

- (void)startEditing:(ReceiptVC *)vc Type:(NSString *)type
{
    [self hideAllResult];

    if(![type isEqualToString:@"discount"])
        [selectedVC setDiscount:selectedVC.product.Discount.intValue];

    selectedVC = vc;
    [self startEditing:vc];
    if([type isEqualToString:@"name"])
        [self hideKeyboardWithAnimation:YES];
    else
    {
        [self.view endEditing:YES];
        [self showKeyboard:type];
    }
}

- (void)startEditing:(ReceiptVC *)vc
{
    [self enableReset];
    [self updateCompareButton];
    
    grayView.frame = CGRectMake(0, 0, receiptsView.contentSize.width + receiptsView.frame.size.width, receiptsView.contentSize.height);
    grayView.hidden = NO;
    [receiptsView bringSubviewToFront:grayView];
    [receiptsView bringSubviewToFront:vc.view];
    
    receiptsView.scrollEnabled = NO;
    
    [receiptsView setContentOffset:CGPointMake(vc.view.frame.origin.x-90, 0) animated:YES];
}

- (IBAction)endEditing
{
    [self.view endEditing:YES];
    grayView.hidden = YES;
    //keyboardView.hidden = YES;
    [self hideKeyboardWithAnimation:YES];
    receiptsView.scrollEnabled = YES;
    
    if([keyboardType isEqualToString:@"discount"])
        [selectedVC setDiscount:selectedVC.product.Discount.intValue];
    
    if(receiptsView.contentOffset.x > receiptsView.contentSize.width - receiptsView.frame.size.width)
        [receiptsView setContentOffset:CGPointMake(receiptsView.contentSize.width - receiptsView.frame.size.width, 0) animated:YES];
    else if(receiptsView.contentOffset.x < 0)
        [receiptsView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    selectedVC = nil;
}

- (void)hideKeyboardWithAnimation:(BOOL)animate
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.height;

    CGRect frame = keyboardView.frame;
    if(frame.origin.x == screenWidth)
        return;
    frame.origin.x = screenWidth;

    if(animate)
    {
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.2];
        keyboardView.frame = frame;
        [UIView commitAnimations];
    }
    else
    {
        keyboardView.frame = frame;
    }
}

NSString *keyboardType;
NSString *currentDisplayString;
- (void)showKeyboard:(NSString *)type
{
//    currentDisplayString = @"";
    keyboardType = type;
    if([keyboardType isEqualToString:@"discount"])
        currentDisplayString = [NSString stringWithFormat:@"%d", selectedVC.product.Discount.intValue];
    else if([keyboardType isEqualToString:@"qty"])
        currentDisplayString = [NSString stringWithFormat:@"%g", selectedVC.product.UnitAmount.floatValue];
    else if([keyboardType isEqualToString:@"price"])
    {
        currentDisplayString = [NSString stringWithFormat:@"%g", selectedVC.product.Price.floatValue];
        NSLog(@"last price: %@", currentDisplayString);
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.height;
    
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.2];
    CGRect frame = keyboardView.frame;
    frame.origin.x = screenWidth - frame.size.width;
    keyboardView.frame = frame;
    [UIView commitAnimations];
    
    if([type isEqualToString:@"discount"])
    {
        [selectedVC setDiscountEditing:selectedVC.product.Discount.intValue];
        
        keyboardButtonDot.enabled = NO;
        keyboardButtonTypeWgt.enabled = NO;
        keyboardButtonTypeVol.enabled = NO;
        keyboardButtonTypeQty.enabled = NO;
        keyboardButtonTypeName1.enabled = NO;
        keyboardButtonTypeName2.enabled = NO;
        keyboardButtonTypeName3.enabled = NO;
        keyboardButtonTypeName4.enabled = NO;
    }
    else if([type isEqualToString:@"qty"])
    {
        keyboardButtonDot.enabled = YES;
        keyboardButtonTypeWgt.enabled = YES;
        keyboardButtonTypeVol.enabled = YES;
        keyboardButtonTypeQty.enabled = YES;
        keyboardButtonTypeName1.enabled = YES;
        keyboardButtonTypeName2.enabled = YES;
        keyboardButtonTypeName3.enabled = YES;
        keyboardButtonTypeName4.enabled = YES;
        
        keyboardButtonTypeName1.selected = [selectedVC.product.UnitName isEqualToString:keyboardButtonTypeName1.currentTitle] ? YES : NO;
        keyboardButtonTypeName2.selected = [selectedVC.product.UnitName isEqualToString:keyboardButtonTypeName2.currentTitle] ? YES : NO;
        keyboardButtonTypeName3.selected = [selectedVC.product.UnitName isEqualToString:keyboardButtonTypeName3.currentTitle] ? YES : NO;
        keyboardButtonTypeName4.selected = [selectedVC.product.UnitName isEqualToString:keyboardButtonTypeName4.currentTitle] ? YES : NO;
        
        keyboardButtonTypeWgt.selected = NO;
        keyboardButtonTypeVol.selected = NO;
        keyboardButtonTypeQty.selected = NO;
        if([selectedVC.product.UnitType isEqualToString:TypeWeight])
            keyboardButtonTypeWgt.selected = YES;
        else if([selectedVC.product.UnitType isEqualToString:TypeVolume])
            keyboardButtonTypeVol.selected = YES;
        else if([selectedVC.product.UnitType isEqualToString:TypeQty])
            keyboardButtonTypeQty.selected = YES;
    }
    else if([type isEqualToString:@"price"])
    {
        keyboardButtonDot.enabled = YES;
        keyboardButtonTypeWgt.enabled = NO;
        keyboardButtonTypeVol.enabled = NO;
        keyboardButtonTypeQty.enabled = NO;
        keyboardButtonTypeName1.enabled = NO;
        keyboardButtonTypeName2.enabled = NO;
        keyboardButtonTypeName3.enabled = NO;
        keyboardButtonTypeName4.enabled = NO;
    }
    
    [self updateKeyboardDisplay];
}

- (void)updateKeyboardDisplay
{
    if([keyboardType isEqualToString:@"discount"])
    {
        keyboardDisplay.text = [NSString stringWithFormat:@"%d%%", selectedVC.product.Discount.intValue];
        [selectedVC setDiscountEditing:selectedVC.product.Discount.intValue];
    }
    else if([keyboardType isEqualToString:@"qty"])
    {
        keyboardDisplay.text = [NSString stringWithFormat:@"%g%@", selectedVC.product.UnitAmount.floatValue, selectedVC.product.UnitName];
        [selectedVC setQty:selectedVC.product.UnitAmount.floatValue unit:selectedVC.product.UnitName];
    }
    else if([keyboardType isEqualToString:@"price"])
    {
        keyboardDisplay.text = [NSString stringWithFormat:@"%@%.2f", [dm getCurrency], selectedVC.product.Price.floatValue];
        [selectedVC setPrice:selectedVC.product.Price.floatValue];
    }
}

#pragma mark - keyboard

- (IBAction)keyboardClicked:(UIButton *)sender
{
    if((sender == keyboardButtonTypeWgt || sender == keyboardButtonTypeVol || sender == keyboardButtonTypeQty) && !sender.selected)
    {
        selectedTypeButton = sender;
        NSString *message = [NSString stringWithFormat:@"All receipt will be change into %@ format without conversion.\nAre you sure you want to continue?", (sender == keyboardButtonTypeWgt ? TypeWeight : (sender == keyboardButtonTypeVol ? TypeVolume : TypeQty)).uppercaseString];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Yes", nil];
        alert.tag = kAlertTagChangeType;
        [alert show];
    }
    else if(sender == keyboardButtonTypeName1 && !sender.selected)
    {
        keyboardButtonTypeName1.selected = YES;
        keyboardButtonTypeName2.selected = NO;
        keyboardButtonTypeName3.selected = NO;
        keyboardButtonTypeName4.selected = NO;
        selectedVC.product.UnitName = sender.currentTitle;
    }
    else if(sender == keyboardButtonTypeName2 && !sender.selected)
    {
        keyboardButtonTypeName1.selected = NO;
        keyboardButtonTypeName2.selected = YES;
        keyboardButtonTypeName3.selected = NO;
        keyboardButtonTypeName4.selected = NO;
        selectedVC.product.UnitName = sender.currentTitle;
    }
    else if(sender == keyboardButtonTypeName3 && !sender.selected)
    {
        keyboardButtonTypeName1.selected = NO;
        keyboardButtonTypeName2.selected = NO;
        keyboardButtonTypeName3.selected = YES;
        keyboardButtonTypeName4.selected = NO;
        selectedVC.product.UnitName = sender.currentTitle;
    }
    else if(sender == keyboardButtonTypeName4 && !sender.selected)
    {
        keyboardButtonTypeName1.selected = NO;
        keyboardButtonTypeName2.selected = NO;
        keyboardButtonTypeName3.selected = NO;
        keyboardButtonTypeName4.selected = YES;
        selectedVC.product.UnitName = sender.currentTitle;
    }
    
    else if(sender == keyboardButtonClear)
    {
        currentDisplayString = @"";
    }
    else if(sender == keyboardButtonBackspace)
    {
        if ([currentDisplayString length] > 0)
            currentDisplayString = [currentDisplayString substringToIndex:[currentDisplayString length] - 1];
    }
    else if(sender == keyboardButton0 || sender == keyboardButton1 || sender == keyboardButton2 || sender == keyboardButton3 || sender == keyboardButton4 || sender == keyboardButton5 || sender == keyboardButton6 || sender == keyboardButton7 || sender == keyboardButton8 || sender == keyboardButton9 || sender == keyboardButtonDot)
    {
        if([currentDisplayString isEqualToString:@"0"])
            currentDisplayString = @"";
        NSString * added = @"0";
        if(sender == keyboardButton1)
            added = @"1";
        else if(sender == keyboardButton2)
            added = @"2";
        else if(sender == keyboardButton3)
            added = @"3";
        else if(sender == keyboardButton4)
            added = @"4";
        else if(sender == keyboardButton5)
            added = @"5";
        else if(sender == keyboardButton6)
            added = @"6";
        else if(sender == keyboardButton7)
            added = @"7";
        else if(sender == keyboardButton8)
            added = @"8";
        else if(sender == keyboardButton9)
            added = @"9";
        else if(sender == keyboardButtonDot)
            added = @".";
        NSString *newStr = [currentDisplayString stringByAppendingString:added];
        if([self isValid:newStr])
            currentDisplayString = newStr;
    }

    [self updateSelectedProduct];
    //[self updateKeyboardDisplay];
    [self updateCompareButton];
}

- (void)updateKeyboardUnitType
{
    if(keyboardButtonTypeWgt.selected)
    {
        [keyboardButtonTypeName1 setTitle:@"g" forState:UIControlStateNormal];
        [keyboardButtonTypeName2 setTitle:@"oz" forState:UIControlStateNormal];
        [keyboardButtonTypeName3 setTitle:@"lb" forState:UIControlStateNormal];
        [keyboardButtonTypeName4 setTitle:@"kg" forState:UIControlStateNormal];
        [selectedVC setQtyType:@"Weight"];
    }
    else if(keyboardButtonTypeVol.selected)
    {
        [keyboardButtonTypeName1 setTitle:@"cc" forState:UIControlStateNormal];
        [keyboardButtonTypeName2 setTitle:@"l" forState:UIControlStateNormal];
        [keyboardButtonTypeName3 setTitle:@"qt" forState:UIControlStateNormal];
        [keyboardButtonTypeName4 setTitle:@"gal" forState:UIControlStateNormal];
        //[keyboardButtonTypeName4 setTitle:@"bbl" forState:UIControlStateNormal];
        [selectedVC setQtyType:@"Volume"];
    }
    else if(keyboardButtonTypeQty.selected)
    {
        [keyboardButtonTypeName1 setTitle:@"pcs" forState:UIControlStateNormal];
        [keyboardButtonTypeName2 setTitle:@"pair" forState:UIControlStateNormal];
        [keyboardButtonTypeName3 setTitle:@"dz" forState:UIControlStateNormal];
        [keyboardButtonTypeName4 setTitle:@"gr" forState:UIControlStateNormal];
        [selectedVC setQtyType:@"Quantity"];
    }

    keyboardButtonTypeName1.selected = NO;
    keyboardButtonTypeName2.selected = NO;
    keyboardButtonTypeName3.selected = NO;
    keyboardButtonTypeName4.selected = NO;
    
    [self keyboardClicked:keyboardButtonTypeName1];
    
    for(ReceiptVC *vc in receiptArray)
    {
        if(selectedVC == vc)
            continue;
        [vc setQtyType:selectedVC.product.UnitType];
        [vc setQty:vc.product.UnitAmount.floatValue unit:keyboardButtonTypeName1.currentTitle];
    }
}
- (BOOL)isValid:(NSString *)string
{
    if([keyboardType isEqualToString:@"discount"] && string.intValue >= 100)
    {
        return  NO;
    }
//    else if([keyboardType isEqualToString:@"qty"] && string.intValue >= 10000)
//    {
//        return NO;
//    }
    else if([keyboardType isEqualToString:@"price"] || [keyboardType isEqualToString:@"qty"])
    {
        NSArray *arr = [string componentsSeparatedByString:@"."];
        if(arr.count > 2)
            return NO;
        if(arr.count > 1)
        {
            NSString *decimalStr = [arr objectAtIndex:1];
            if(decimalStr.length > 2)
                return NO;
        }
        if(arr.count > 0)
        {
            NSString *decimalStr = [arr objectAtIndex:0];
//            NSLog(@"%@ [%lu]", decimalStr, (unsigned long)decimalStr.length);
            if(decimalStr.length > 4)
                return NO;
            
//            if([keyboardType isEqualToString:@"price"] && decimalStr.length > 4)
//                return NO;
//            else if([keyboardType isEqualToString:@"qty"] && decimalStr.length > 5)
//                return NO;
        }
    }
    
    return YES;
}

- (IBAction)confirmKeyboardTypeClicked:(UIButton *)sender
{
    if(sender == keyboardButtonTypeWgt && !sender.selected)
    {
        keyboardButtonTypeWgt.selected = YES;
        keyboardButtonTypeVol.selected = NO;
        keyboardButtonTypeQty.selected = NO;
        [self updateKeyboardUnitType];
    }
    else if(sender == keyboardButtonTypeVol && !sender.selected)
    {
        keyboardButtonTypeWgt.selected = NO;
        keyboardButtonTypeVol.selected = YES;
        keyboardButtonTypeQty.selected = NO;
        [self updateKeyboardUnitType];
    }
    else if(sender == keyboardButtonTypeQty && !sender.selected)
    {
        keyboardButtonTypeWgt.selected = NO;
        keyboardButtonTypeVol.selected = NO;
        keyboardButtonTypeQty.selected = YES;
        [self updateKeyboardUnitType];
    }
}

- (void)updateSelectedProduct
{
    if(currentDisplayString.length == 0)
    {
        if([keyboardType isEqualToString:@"discount"])
        {
            selectedVC.product.Discount = 0;
        }
        else if([keyboardType isEqualToString:@"qty"])
        {
            selectedVC.product.UnitAmount = 0;
        }
        else if([keyboardType isEqualToString:@"price"])
        {
            selectedVC.product.Price = 0;
        }
    }
    else
    {
        if([keyboardType isEqualToString:@"discount"])
        {
            selectedVC.product.Discount = [NSNumber numberWithInt:currentDisplayString.intValue];
        }
        else if([keyboardType isEqualToString:@"qty"])
        {
            selectedVC.product.UnitAmount = [NSNumber numberWithFloat:currentDisplayString.floatValue];
        }
        else if([keyboardType isEqualToString:@"price"])
        {
            selectedVC.product.Price = [NSNumber numberWithFloat:currentDisplayString.floatValue];
        }
    }
    
    [self updateKeyboardDisplay];
}

#pragma mark - Alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//cancel
        return;
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if(alertView.tag == kAlertTagChangeType && [title isEqualToString:@"Yes"])
    {
        [self confirmKeyboardTypeClicked:selectedTypeButton];
    }
    else if(alertView.tag == kAlertTagCurrencySymbol && [title isEqualToString:@"OK"])
    {
        UITextField *textfield = [alertView textFieldAtIndex:0];
        [dm setCurrency:textfield.text];
    }

}


#pragma mark - Barcode
- (void)scanBarcode:(ReceiptVC *)vc
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) //Check if camera is available
        return;
    
    selectedVC = vc;
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
//    float currentVersion = 5.1;
//    float sysVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
//    
//    UIView * infoButton;
//    if (sysVersion > currentVersion)
//        infoButton = [[[[[reader.view.subviews objectAtIndex:1] subviews] objectAtIndex:0] subviews] objectAtIndex:3];
//    else
//        infoButton = [[[[[reader.view.subviews objectAtIndex:1] subviews] objectAtIndex:0] subviews] objectAtIndex:2];
//    [infoButton setHidden:YES];
    
    UIView * infoButton = [[[[[reader.view.subviews objectAtIndex:2] subviews] objectAtIndex:0] subviews] objectAtIndex:3];
    
    [infoButton setHidden:YES];
    
    // present and release the controller
    [self presentViewController:reader animated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Barcode"          // Event label
                                                           value:nil] build]];
    
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;

    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [reader dismissViewControllerAnimated:YES completion:nil];

    // EXAMPLE: do something useful with the barcode data
//    resultText.text = symbol.data;
//    
//    // EXAMPLE: do something useful with the barcode image
//    resultImage.image =
//    [info objectForKey: UIImagePickerControllerOriginalImage];
    //NSLog(@"type: %d", symbol.type);
    if(symbol.type == ZBAR_EAN13)
    {
        NSLog(@"%@", symbol.data);
        [selectedVC setBarcode:symbol.data];
        
        //get product name
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            UPCDatabase *upc = [dm GetProduct:symbol.data];
            if(!upc.Valid)
            {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:upc.Reason message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
                [WToast showWithText:upc.Reason duration:kWTShort];
            }
            else
            {
                if(upc.ItemName.length > 0)
                    [selectedVC setProductName:upc.ItemName];
                if(upc.Description.length > 0)
                    [selectedVC setProductDescription:upc.Description];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        });
    }
    else
    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid code" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
        [WToast showWithText:@"Invalid code" duration:kWTShort];

    }
    
}

#pragma mark -
- (void)updateCompareButton
{
    [self hideAllResult];
    if(receiptArray.count < 1)
    {
        compareButton.enabled = NO;
        return;
    }
    for(ReceiptVC *vc in receiptArray)
    {
        Product *product = vc.product;
        if(product.Price.floatValue <= 0.0 || product.UnitAmount.floatValue <= 0)
        {
            compareButton.enabled = NO;
            return;
        }
    }
    
    compareButton.enabled = YES;
}

- (void)hideAllResult
{
    for(ReceiptVC *vc in receiptArray)
    {
        [vc hideCrown];
    }
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 2) ? NO : YES;
}

#pragma mark - send email
- (void)sendMailTo:(NSString *)messageTo subject:(NSString *)messageSubject body:(NSString *)messageBody
{
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if([mailClass canSendMail])
		//if(FALSE)
	{
		NSLog(@"mail versi 3.0");
		MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
		mailCompose.mailComposeDelegate = self;
		if(messageTo != nil)
			[mailCompose setToRecipients:[NSArray arrayWithObject:messageTo]];
		
		[mailCompose setSubject:messageSubject];
		
		//[mailCompose setMessageBody:messageBody isHTML:NO];
		[mailCompose setMessageBody:messageBody isHTML:YES];
		
        //NSLog(@"-> %.2f %.2f", myView.frame.origin.y , myView.frame.size.height);
        [self presentViewController:mailCompose animated:YES completion:nil];
	}
	else
    {
		NSLog(@"mail fail");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts"
                                                        message:@"There are no mail account configured. You can add or create a mail account in Settings"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dm     = [DataManager sharedInstance];
    
    compareWeightArr = [[NSMutableArray alloc] init];
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"g", @"UnitName", [NSNumber numberWithFloat:1.0], @"UnitMultiply", nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"oz", @"UnitName", [NSNumber numberWithFloat:28.3495], @"UnitMultiply", nil];
    NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"lb", @"UnitName", [NSNumber numberWithFloat:453.592], @"UnitMultiply", nil];
    NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"kg", @"UnitName", [NSNumber numberWithFloat:1000.0], @"UnitMultiply", nil];
    [compareWeightArr addObject:dic1];
    [compareWeightArr addObject:dic2];
    [compareWeightArr addObject:dic3];
    [compareWeightArr addObject:dic4];
    
    compareVolumeArr = [[NSMutableArray alloc] init];
    dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"cc", @"UnitName", [NSNumber numberWithFloat:1.0], @"UnitMultiply", nil];
    dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"l", @"UnitName", [NSNumber numberWithFloat:1000.0], @"UnitMultiply", nil];
    dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"gal", @"UnitName", [NSNumber numberWithFloat:3785.41], @"UnitMultiply", nil];
//    dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"bbl", @"UnitName", [NSNumber numberWithFloat:159000.0], @"UnitMultiply", nil];
    dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"qt", @"UnitName", [NSNumber numberWithFloat:159000.0], @"UnitMultiply", nil];
    [compareVolumeArr addObject:dic1];
    [compareVolumeArr addObject:dic2];
    [compareVolumeArr addObject:dic3];
    [compareVolumeArr addObject:dic4];
    
    compareQtyArr = [[NSMutableArray alloc] init];
    dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"pcs", @"UnitName", [NSNumber numberWithFloat:1.0], @"UnitMultiply", nil];
    dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"pair", @"UnitName", [NSNumber numberWithFloat:2.0], @"UnitMultiply", nil];
    dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"dz", @"UnitName", [NSNumber numberWithFloat:12.0], @"UnitMultiply", nil];
    dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"gr", @"UnitName", [NSNumber numberWithFloat:144.0], @"UnitMultiply", nil];
    [compareQtyArr addObject:dic1];
    [compareQtyArr addObject:dic2];
    [compareQtyArr addObject:dic3];
    [compareQtyArr addObject:dic4];
    
    receiptArray = [[NSMutableArray alloc] init];
    [self hideKeyboardWithAnimation:NO];
    [self endEditing];
    
    [currencyButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    initialize = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveData) name:kNotificationSaveData object:nil];
}

- (void)saveData
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(ReceiptVC *vc in receiptArray)
        [arr addObject:vc.product];
    [dm saveData:arr];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Main Screen";
}

bool initialize;
- (void)viewDidAppear:(BOOL)animated
{
    if(!initialize)
    {
        NSArray *arr = dm.loadData;
        if(!arr || arr.count == 0)
            [self resetClicked:nil];
        else
            [self loadData:arr];
        initialize = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
