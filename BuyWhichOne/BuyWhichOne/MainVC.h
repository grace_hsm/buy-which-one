//
//  MainVC.h
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "GAITrackedViewController.h"

@interface MainVC : GAITrackedViewController <ZBarReaderDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet UIView *keyboardView;
    IBOutlet UILabel *keyboardDisplay;
    IBOutlet UIButton *keyboardButton1;
    IBOutlet UIButton *keyboardButton2;
    IBOutlet UIButton *keyboardButton3;
    IBOutlet UIButton *keyboardButton4;
    IBOutlet UIButton *keyboardButton5;
    IBOutlet UIButton *keyboardButton6;
    IBOutlet UIButton *keyboardButton7;
    IBOutlet UIButton *keyboardButton8;
    IBOutlet UIButton *keyboardButton9;
    IBOutlet UIButton *keyboardButton0;
    IBOutlet UIButton *keyboardButtonClear;
    IBOutlet UIButton *keyboardButtonDot;
    IBOutlet UIButton *keyboardButtonTypeWgt;
    IBOutlet UIButton *keyboardButtonTypeVol;
    IBOutlet UIButton *keyboardButtonTypeQty;
    IBOutlet UIButton *keyboardButtonTypeName1;
    IBOutlet UIButton *keyboardButtonTypeName2;
    IBOutlet UIButton *keyboardButtonTypeName3;
    IBOutlet UIButton *keyboardButtonTypeName4;
    IBOutlet UIButton *keyboardButtonBackspace;
    
    IBOutlet UIButton *compareButton;
    IBOutlet UIButton *resetButton;
    IBOutlet UIButton *soundButton;
    IBOutlet UIButton *currencyButton;

    IBOutlet UIButton *plusButton;

    IBOutlet UIScrollView *receiptsView;
    IBOutlet UIControl *grayView;
    NSMutableArray *receiptArray;
    
    NSMutableArray *compareWeightArr;
    NSMutableArray *compareVolumeArr;
    NSMutableArray *compareQtyArr;
}


@end
