//
//  DataManager.h
//  BuyWhichOne
//
//  Created by Grace on 15/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UPCDatabase.h"

#define GameTitle @"Buy Which One"
#define kNotificationCurrencyUpdated @"currencyUpdated"
#define kNotificationSaveData @"saveData"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface DataManager : NSObject
{
    NSURLConnection *theConnection;
}

@property (retain, nonatomic) NSMutableData *responseData;

+ (id) sharedInstance;
- (void) initialize;

- (BOOL)showAds;
- (id)GetProduct:(NSString *)code;

- (NSString *)getCurrency;
- (void)setCurrency:(NSString *)string;

- (void)saveData:(NSMutableArray *)products;
- (NSArray *)loadData;

@end
