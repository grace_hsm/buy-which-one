//
//  AdsVC.h
//  BuyWhichOne
//
//  Created by Grace on 19/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface AdsVC : GAITrackedViewController
{
    IBOutlet UIImageView *adImage;
}
@end
