//
//  UPCDatabase.h
//  BuyWhichOne
//
//  Created by Grace on 15/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UPCDatabase : NSObject

@property (nonatomic) bool Valid;
@property (nonatomic, retain) NSString *Number;
@property (nonatomic, retain) NSString *ItemName;

@property (nonatomic, retain) NSString *Description;
@property (nonatomic) float Price;

@property (nonatomic, retain) NSString *Reason;


@end
