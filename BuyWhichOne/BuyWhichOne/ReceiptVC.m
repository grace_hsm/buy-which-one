//
//  ReceiptVC.m
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import "ReceiptVC.h"

#import "ZXMultiFormatWriter.h"
#import "ZXBitMatrix.h"
#import "ZXImage.h"
#import "DataManager.h"

@interface ReceiptVC ()

@end

//ZXMultiFormatWriter *writer = [ZXMultiFormatWriter writer];
//ZXBitMatrix* result
@implementation ReceiptVC
@synthesize delegate, productNameTextfield, product;

DataManager *dm;

- (IBAction)endEditing
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(endEditing)])
    {
        [self.delegate performSelector:@selector(endEditing) withObject:nil];
    }
}

- (IBAction)infoClicked:(id)sender
{
    [self FlipToBackView];
}
- (IBAction)resultClicked:(id)sender
{
    [self FlipToFrontView];
}
- (IBAction)discountClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(startEditing:Type:)])
    {
        [self.delegate performSelector:@selector(startEditing:Type:) withObject:self withObject:@"discount"];
    }
}
- (IBAction)qtyClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(startEditing:Type:)])
    {
        [self.delegate performSelector:@selector(startEditing:Type:) withObject:self withObject:@"qty"];
    }
}
- (IBAction)priceClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(startEditing:Type:)])
    {
        [self.delegate performSelector:@selector(startEditing:Type:) withObject:self withObject:@"price"];
    }
}
- (IBAction)barcodeClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scanBarcode:)])
    {
        [self.delegate performSelector:@selector(scanBarcode:) withObject:self withObject:@"price"];
    }
}

#pragma mark -
- (void)setProduct:(Product *)newProduct
{
    product = newProduct;
    [self setProductName:newProduct.Name];
    [self setProductPlaceHolder:newProduct.PlaceHolder];
    [self setProductDescription:newProduct.Description];
    [self setDiscount:newProduct.Discount.intValue];
    [self setPrice:newProduct.Price.floatValue];
    [self setQtyType:newProduct.UnitType];
    [self setQty:newProduct.UnitAmount.floatValue unit:newProduct.UnitName];
    [self setBarcode:newProduct.codeString];
}

- (void)setBarcode:(NSString *)codeString
{
    product.codeString = codeString;
    
    NSError *error = nil;
    ZXMultiFormatWriter *writer = [ZXMultiFormatWriter writer];
    ZXBitMatrix* result = [writer encode:product.codeString
                                  format:kBarcodeFormatEan13
                                   width:barcodeImageView.frame.size.width
                                  height:barcodeImageView.frame.size.height
                                   error:&error];
    if (result) {
        UIImage* generatedImage = [[UIImage alloc] initWithCGImage:[[ZXImage imageWithMatrix:result] cgimage]];
        barcodeImageView.image = generatedImage;
        barcodeLabel.text = product.codeString;
        // This CGImageRef image can be placed in a UIImage, NSImage, or written to a file.
    } else {
        NSString *errorMessage = [error localizedDescription];
        NSLog(@"error: %@", errorMessage);
    }
}

- (void)setProductName:(NSString *)name
{
    product.Name = name;
    productNameTextfield.text = name;
}
- (void)setProductPlaceHolder:(NSString *)placeHolder
{
    product.PlaceHolder = placeHolder;
    productNameTextfield.placeholder = placeHolder;
}
- (void)setProductDescription:(NSString *)name
{
    product.Description = name;
    productDescriptionTextfield.text = name;
}

- (void)setDiscount:(int)new
{
    product.Discount = [NSNumber numberWithInt:new];
    if(product.Discount.intValue > 0)
    {
        [self setDiscountEditing:new];
    }
    else
    {
        noDiscountButton.hidden = NO;
        discountView.hidden = YES;
        
        [self hidePriceCompare];
    }
}

- (void)setDiscountEditing:(int)new
{
    product.Discount = [NSNumber numberWithInt:new];
    discountLabel.text = [NSString stringWithFormat:@"%d%%", product.Discount.intValue];
        
    noDiscountButton.hidden = YES;
    discountView.hidden = NO;

//    float NewUP = product.UsualPrice;
//    product.Price = product.UsualPrice * 100 / (100-product.Discount);
//    [self setPrice:product.Price];
//    [self setPriceCompare:NewUP];
    
    float NewUP = product.UsualPrice.floatValue;
    product.Price = [NSNumber numberWithFloat:(product.UsualPrice.floatValue * (100-product.Discount.intValue) / 100)];
    [self setPrice:product.Price.floatValue];
    [self setPriceCompare:NewUP];
}

- (void)setPrice:(float)new
{
    if(product.Discount.intValue > 0)
    {
        product.Price = [NSNumber numberWithFloat:new];
        priceLabel.text = [NSString stringWithFormat:@"%.2f", product.Price.floatValue];
        
//        float up = product.Price * (100+product.Discount) / 100;
        float up = product.Price.floatValue * 100 / (100-product.Discount.intValue);
        product.UsualPrice = [NSNumber numberWithFloat:up];
        [self setPriceCompare:up];
        
//        product.UsualPrice = new;
//        float actualPrice = product.UsualPrice * (100-product.Discount) / 100;
//        product.Price = actualPrice;
//        priceLabel.text = [NSString stringWithFormat:@"%.2f", product.Price];
//        
//        [self setPriceCompare:product.UsualPrice];
    }
    else
    {
        product.Price = [NSNumber numberWithFloat:new];
        product.UsualPrice = [NSNumber numberWithFloat:new];
        
        priceLabel.text = [NSString stringWithFormat:@"%.2f", product.Price.floatValue];
        [self hidePriceCompare];
    }

    CGSize size = [priceLabel.text sizeWithAttributes:
                   @{NSFontAttributeName:priceLabel.font}];
    float width = size.width;
    
    CGRect frame = currencyLabel.frame;
    frame.origin.x = priceLabel.frame.origin.x + priceLabel.frame.size.width - width - frame.size.width;
    currencyLabel.frame = frame;

}

- (void)setQtyType:(NSString *)type
{
    product.UnitType = type;
    qtyTypeLabel.text = [NSString stringWithFormat:@"%@", product.UnitType];
}
- (void)setQty:(float)value unit:(NSString *)unit
{
    product.UnitAmount = [NSNumber numberWithFloat:value];
    product.UnitName = unit;
    qtyValueLabel.text = [NSString stringWithFormat:@"%g%@", product.UnitAmount.floatValue, product.UnitName];
}

- (void)setPriceCompare:(float)new
{
    product.UsualPrice = [NSNumber numberWithFloat:new];
    if(new > 0 && product.Discount.intValue > 0)
    {
        priceCompareLabel.text = [NSString stringWithFormat:@"%@%.2f", [dm getCurrency], new];
        priceCompareView.hidden = NO;
    }
    else
        priceCompareView.hidden = YES;
}
- (void)hidePriceCompare
{
    priceCompareView.hidden = YES;
}

- (void)setCrown:(int)order
{
    NSString *imagename = [NSString stringWithFormat:@"crown%d.png", order];
    crownImage.image = [UIImage imageNamed:imagename];
    crownImage.hidden = NO;
    
    infoButton.hidden = NO;
    
    resultProductNameTextfield.text = productNameTextfield.text;
    resultProductDescriptionTextfield.text = productDescriptionTextfield.text;
    resultPriceLabel.text = [NSString stringWithFormat:@"Price:\n%@%.2f/%@", [dm getCurrency], self.product.PriceResult.floatValue, self.product.UnitNameResult];
}
- (void)hideCrown
{
    crownImage.hidden = YES;
    infoButton.hidden = YES;
    
    [self FlipToFrontView];
}

- (void)FlipToBackView
{
    if(frontView.hidden)
        return;
    
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:myView cache:YES];
	resultView.hidden = NO;
    frontView.hidden = YES;
	[UIView commitAnimations];
}
- (void)FlipToFrontView
{
    if(resultView.hidden)
        return;
    
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:myView cache:YES];
	resultView.hidden = YES;
    frontView.hidden = NO;
	[UIView commitAnimations];
}

#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dm     = [DataManager sharedInstance];
    
    currencyLabel.text = dm.getCurrency;
    resultView.hidden = YES;
    [self hideCrown];
    
    UISwipeGestureRecognizer * swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpAction)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
//    swipeUp.delegate = self;
    [self.view addGestureRecognizer:swipeUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCurrencySymbol) name:kNotificationCurrencyUpdated object:nil];
}

- (void)refreshCurrencySymbol
{
    currencyLabel.text = dm.getCurrency;
    if(!priceCompareView.hidden)
        priceCompareLabel.text = [NSString stringWithFormat:@"%@%.2f", [dm getCurrency], product.UsualPrice.floatValue];
        
}

- (void)swipeUpAction
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(removeReceipt:)])
    {
        [self.delegate performSelector:@selector(removeReceipt:) withObject:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(startEditing:Type:)])
    {
        [self.delegate performSelector:@selector(startEditing:Type:) withObject:self withObject:@"name"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == productNameTextfield)
        [productDescriptionTextfield becomeFirstResponder];
    else if(textField == productDescriptionTextfield)
    {
//        [self.view endEditing:YES];
        if(self.delegate && [self.delegate respondsToSelector:@selector(endEditing)])
        {
            [self.delegate performSelector:@selector(endEditing) withObject:nil];
        }
    }
    return NO;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == productNameTextfield)
        product.Name = textField.text;
    else if(textField == productDescriptionTextfield)
        product.Description = textField.text;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
