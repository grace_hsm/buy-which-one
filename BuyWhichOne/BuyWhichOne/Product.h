//
//  Product.h
//  BuyWhichOne
//
//  Created by Grace on 7/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, retain) NSString *Name;
@property (nonatomic, retain) NSString *PlaceHolder;
@property (nonatomic, retain) NSString *Description;

@property (nonatomic, retain) NSString *UnitType; //Weight/Volume/Qty
@property (nonatomic, retain) NSNumber *UnitAmount; //float
@property (nonatomic, retain) NSString *UnitName;

@property (nonatomic, retain) NSNumber *Discount; //int
@property (nonatomic, retain) NSNumber *Price; //float
@property (nonatomic, retain) NSNumber *UsualPrice; //float

@property (nonatomic, retain) NSNumber *PriceResult; //float
@property (nonatomic, retain) NSString *UnitNameResult;
@property (nonatomic, retain) NSNumber *Ranking; // ranking

@property (nonatomic, retain) NSString *codeString;

@end
