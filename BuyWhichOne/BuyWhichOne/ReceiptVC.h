//
//  ReceiptVC.h
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

#define TypeWeight @"Weight"
#define TypeVolume @"Volume"
#define TypeQty @"Quantity"


@interface ReceiptVC : UIViewController
{
    IBOutlet UIView *myView;

    IBOutlet UIView *frontView;
    IBOutlet UIImageView *crownImage;
    
    IBOutlet UITextField *productNameTextfield;
    IBOutlet UITextField *productDescriptionTextfield;
    IBOutlet UIButton *infoButton;
    
    IBOutlet UILabel *qtyTypeLabel;
    IBOutlet UILabel *qtyValueLabel;

    IBOutlet UIButton *noDiscountButton;
    IBOutlet UIView *discountView;
    IBOutlet UILabel *discountLabel;

    IBOutlet UIImageView *barcodeImageView;
    IBOutlet UILabel *barcodeLabel;

    IBOutlet UILabel *priceLabel;
    IBOutlet UILabel *currencyLabel;

    IBOutlet UIView *priceCompareView;
    IBOutlet UILabel *priceCompareLabel;
    
    IBOutlet UIView *resultView;
    IBOutlet UITextField *resultProductNameTextfield;
    IBOutlet UITextField *resultProductDescriptionTextfield;
    IBOutlet UILabel *resultPriceLabel;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) IBOutlet UITextField *productNameTextfield;
@property (nonatomic, retain) Product *product;

- (void)setCrown:(int)order;
- (void)hideCrown;

- (void)setBarcode:(NSString *)codeString;
- (void)setProductName:(NSString *)name;
- (void)setProductPlaceHolder:(NSString *)placeHolder;
- (void)setProductDescription:(NSString *)name;
- (void)setDiscount:(int)disc;
- (void)setDiscountEditing:(int)new;
- (void)setPrice:(float)price;
- (void)setQtyType:(NSString *)unit;
- (void)setQty:(float)value unit:(NSString *)unit;


@end
