//
//  ViewController.m
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()

@end

@implementation ViewController

bool shouldAnimate;

- (void)addDots
{
    int width = 7;
    int gap = 4;
    int phase = 1;
    int x = dotsView.frame.size.width - 3;
    int y = 3;
    
    int counter = 0;
    do {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, width)];
        img.image = [UIImage imageNamed:@"dot.png"];
        img.alpha = 0;
        
        [dotsView addSubview:img];
        
        counter ++;
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:counter*0.015];
        img.alpha = 0.7;
        [UIView commitAnimations];
        
        
        if(phase == 1)
        {
            if(x - (width+gap) > 0)
                x -= width+gap;
            else
            {
                phase = 2;
                y += width+gap;
            }
        }
        else if(phase == 2)
        {
            if(y + (width*2+gap) < dotsView.frame.size.height)
                y += width+gap;
            else
            {
                phase = 3;
                x += width+gap;
            }
        }
        else if(phase == 3)
        {
            if(x + (width+gap) < dotsView.frame.size.width)
                x += width+gap;
            else
                phase = 4;
        }
    } while (phase < 4);
    
}

- (void)titleAnimation
{
    for(int i = 0; i<3; i++)
    {
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:i*0.1 + 1.5];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        
        UIImageView *temp;
        if(i ==0)
            temp = title1;
        else if(i ==1)
            temp = title2;
        else
            temp = title3;
        
        CGRect frame = temp.frame;
        frame.origin.x += 230;
        temp.frame = frame;
        
        [UIView commitAnimations];
    }
}

- (void)receiptAnimation
{
    for(int i = 0; i<3; i++)
    {
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:(2-i)*0.3 + 0.3];
        
        UIImageView *temp;
        if(i ==0)
            temp = receipt1;
        else if(i ==1)
            temp = receipt2;
        else
            temp = receipt3;
        
        CGRect frame = temp.frame;
        frame.origin.y -= 300;
        temp.frame = frame;
        
        [UIView commitAnimations];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    shouldAnimate = YES;
    [self hideTitle];
    [self hideReceipt];
}

- (void)hideTitle
{
    for(int i = 0; i<3; i++)
    {
        UIImageView *temp;
        if(i ==0)
            temp = title1;
        else if(i ==1)
            temp = title2;
        else
            temp = title3;

        CGRect frame = temp.frame;
        frame.origin.x -= 230;
        temp.frame = frame;
    }
}

- (void)hideReceipt
{
    for(int i = 0; i<3; i++)
    {
        UIImageView *temp;
        if(i ==0)
            temp = receipt1;
        else if(i ==1)
            temp = receipt2;
        else
            temp = receipt3;
        
        CGRect frame = temp.frame;
        frame.origin.y += 300;
        temp.frame = frame;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if(shouldAnimate)
    {
        [self addDots];
        [self titleAnimation];
        [self receiptAnimation];
        shouldAnimate = NO;
    }
    
    [self performSelector:@selector(nextScreen) withObject:nil afterDelay:3.5];
}

- (void)nextScreen
{
    UIStoryboard* storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
    
//    [self.navigationController pushViewController:vc animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
