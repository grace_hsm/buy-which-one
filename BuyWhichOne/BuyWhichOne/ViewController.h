//
//  ViewController.h
//  BuyWhichOne
//
//  Created by Grace on 6/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UIView *dotsView;
    
    IBOutlet UIImageView *title1;
    IBOutlet UIImageView *title2;
    IBOutlet UIImageView *title3;
    IBOutlet UIImageView *receipt1;
    IBOutlet UIImageView *receipt2;
    IBOutlet UIImageView *receipt3;
    
    
}
@end
