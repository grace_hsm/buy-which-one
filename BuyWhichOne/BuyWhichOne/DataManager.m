//
//  DataManager.m
//  BuyWhichOne
//
//  Created by Grace on 15/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import "DataManager.h"
#import "JSON.h"
#import "Product.h"

@implementation DataManager

@synthesize responseData;

static DataManager *sharedInstance = nil;

+ (DataManager *) sharedInstance
{
    if (sharedInstance == nil)
        sharedInstance = [[super allocWithZone:NULL] init];
    return  sharedInstance;
}

- (void) initialize
{
    
}

- (BOOL)showAds
{
    int counter;
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"ShowAds"]) //not initialized
        counter = 0;
    else
    {
        NSNumber *val = [[NSUserDefaults standardUserDefaults] objectForKey:@"ShowAds"];
        counter = val.intValue;
    }
    
    counter ++;
    if(counter >= 5)
        counter = 0;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:counter] forKey:@"ShowAds"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if(counter == 0)
    {
        return YES;
    }
    return NO;
}


- (id)GetProduct:(NSString *)code
{
    NSString *API_KEY = @"4343c3933bb5758871e51b27a43bd8b9";
    NSString *url = [NSString stringWithFormat:@"http://upcdatabase.org/api/json/%@/%@", API_KEY, code];
    //NSLog(@"url %@", url);
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    if (theConnection)
    {
        [theConnection cancel];
        //[theConnection release];
        theConnection = nil;
    }
    theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    
    if( theConnection )
    {
        if(self.responseData)
        {
            //[self.responseData release];
            self.responseData = nil;
        }
        self.responseData = [NSMutableData data];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
    
    [theConnection cancel];
    theConnection = nil;
    
    NSURLResponse* response;
    NSError* error = nil;
    NSData* result = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
    
    NSString *responseString = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"--- response: %@", responseString);
    
    NSDictionary *dic = [responseString JSONValue];
    UPCDatabase *upc = [[UPCDatabase alloc] init];
    
    upc.Valid = [[dic valueForKey:@"valid"] boolValue];
    if(upc.Valid)
    {
        upc.Number = [dic valueForKey:@"number"];
        upc.ItemName = [dic valueForKey:@"itemname"];
        upc.Description = [dic valueForKey:@"description"];
        upc.Price = [[dic valueForKey:@"price"] floatValue];
    }
    else
    {
        upc.Reason = [dic valueForKey:@"reason"];
        if(!upc.Reason || [upc.Reason isEqual: [NSNull null]] || upc.Reason.length == 0)
            upc.Reason = @"Code not found in database.";
    }
    return upc;
}

- (NSString *)getCurrency
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"CurrencySymbol"]) //not initialized
        return @"$";
    
    NSString *val;
    val  = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrencySymbol"];
    return val;
}
- (void)setCurrency:(NSString *)string
{
    [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"CurrencySymbol"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCurrencyUpdated object:nil];
}

- (void)saveData:(NSMutableArray *)products
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(Product *product in products)
        [arr addObject:[self convertProductToDictionary:product]];
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:@"SaveData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSArray *)loadData
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"SaveData"]) //not initialized
        return nil;
        
    NSArray *val = [[NSUserDefaults standardUserDefaults] objectForKey:@"SaveData"];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(NSDictionary *dic in val)
        [arr addObject:[self convertDictionaryToProduct:dic]];
    
    return arr;
}

- (NSDictionary *)convertProductToDictionary:(Product *)product
{
//    @property (nonatomic, retain) NSString *Name;
//    @property (nonatomic, retain) NSString *Description;
//    
//    @property (nonatomic, retain) NSString *UnitType; //Weight/Volume/Qty
//    @property (nonatomic, retain) NSNumber *UnitAmount; //float
//    @property (nonatomic, retain) NSString *UnitName;
//    
//    @property (nonatomic, retain) NSNumber *Discount; //int
//    @property (nonatomic, retain) NSNumber *Price; //float
//    @property (nonatomic, retain) NSNumber *UsualPrice; //float
//    
//    @property (nonatomic, retain) NSNumber *PriceResult; //float
//    @property (nonatomic, retain) NSString *UnitNameResult;
//    @property (nonatomic, retain) NSNumber *Ranking; // ranking
//    
//    @property (nonatomic, retain) NSString *codeString;
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys: product.Name, @"Name",
                         product.PlaceHolder, @"PlaceHolder",
                         product.Description, @"Description",
                         product.UnitType, @"UnitType",
                         product.UnitAmount, @"UnitAmount",
                         product.UnitName, @"UnitName",
                         product.Discount, @"Discount",
                         product.Price, @"Price",
                         product.UsualPrice, @"UsualPrice",
                         product.codeString, @"codeString",
                         nil];
    return dic;
}

- (Product *)convertDictionaryToProduct:(NSDictionary *)dic
{
    Product *product = [[Product alloc] init];
    product.Name = [dic objectForKey:@"Name"];
    product.PlaceHolder = [dic objectForKey:@"PlaceHolder"];
    product.Description = [dic objectForKey:@"Description"];
    product.UnitType = [dic objectForKey:@"UnitType"];
    product.UnitAmount = [dic objectForKey:@"UnitAmount"];
    product.UnitName = [dic objectForKey:@"UnitName"];
    
    product.Discount = [dic objectForKey:@"Discount"];
    product.Price = [dic objectForKey:@"Price"];
    product.UsualPrice = [dic objectForKey:@"UsualPrice"];
    
    product.codeString = [dic objectForKey:@"codeString"];
    
    return product;
}

@end
