//
//  AdsVC.m
//  BuyWhichOne
//
//  Created by Grace on 19/5/14.
//  Copyright (c) 2014 Grace. All rights reserved.
//

#import "AdsVC.h"
#import "DataManager.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@interface AdsVC ()

@end

@implementation AdsVC

- (IBAction)closeClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)adsClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"     // Event category (required)
                                                          action:@"Button Press"  // Event action (required)
                                                           label:@"Ads"          // Event label
                                                           value:nil] build]];
    
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/en/app/income-expense/id740780560?mt=8"];
    if (![[UIApplication sharedApplication] openURL:url])
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    
    [self closeClicked:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    adImage.image = [UIImage imageNamed:(IS_IPHONE_5 ? @"ad-568.png" : @"ad.png")];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Ads Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
